<!--
SPDX-FileCopyrightText: 2023 Univention GmbH
SPDX-License-Identifier: AGPL-3.0-only
-->
# Univention Keycloak

This repository contains a helm chart for deploying the Keycloak component of the Univention Management Stack.

## Prerequisites

Before you begin, ensure you have met the following requirements:

- Kubernetes 1.21+
- Helm 3.0.0+
- Optional: PV provisioner support in the underlying infrastructure

## Documentation

The documentation is placed in the README of each helm chart:

- [ums-keycloak](charts/ums-keycloak)

## License

This project uses the following license: AGPL-3.0-only

## Copyright

Copyright © 2023 Univention GmbH
