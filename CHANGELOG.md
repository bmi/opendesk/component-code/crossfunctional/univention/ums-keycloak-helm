## [1.0.5](https://gitlab.souvap-univention.de/souvap/tooling/charts/univention-keycloak/compare/v1.0.4...v1.0.5) (2024-02-26)


### Bug Fixes

* **envvar:** Remove UNIVENTION_META_JSON as it is addressed using Ingress in Keycloak-Extension's Proxy ([a0600e8](https://gitlab.souvap-univention.de/souvap/tooling/charts/univention-keycloak/commit/a0600e8e7d10d59d61972ac2229fe7f5abe0e23d))

## [1.0.4](https://gitlab.souvap-univention.de/souvap/tooling/charts/univention-keycloak/compare/v1.0.3...v1.0.4) (2024-02-26)


### Bug Fixes

* **envvar:** Set UNIVENTION_META_JSON ([b04a673](https://gitlab.souvap-univention.de/souvap/tooling/charts/univention-keycloak/commit/b04a6735d086f8526ed52a8e554d99966fc5b115))

## [1.0.3](https://gitlab.souvap-univention.de/souvap/tooling/charts/univention-keycloak/compare/v1.0.2...v1.0.3) (2023-12-22)


### Bug Fixes

* **keycloak:** Fix ENV var name for setting the log level ([59895a9](https://gitlab.souvap-univention.de/souvap/tooling/charts/univention-keycloak/commit/59895a9c880f238d2c5de7643e3492590d44e5dc))

## [1.0.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/univention-keycloak/compare/v1.0.1...v1.0.2) (2023-12-22)


### Bug Fixes

* **keycloak:** Add ENV variables to support password renewal and setting the logLevel ([be192f5](https://gitlab.souvap-univention.de/souvap/tooling/charts/univention-keycloak/commit/be192f5de32bf66d09b723c807efb26f7b3444f7))

## [1.0.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/univention-keycloak/compare/v1.0.0...v1.0.1) (2023-12-13)


### Bug Fixes

* **keycloak:** Enable metrics by default, template ports and extend optional ingress (which is by default disabled) ([e905fd1](https://gitlab.souvap-univention.de/souvap/tooling/charts/univention-keycloak/commit/e905fd168dbf0f977159f2f936569a2c86d45ac2))

# 1.0.0 (2023-11-28)


### Bug Fixes

* **keycloak:** Initial commit ([a56d5c5](https://gitlab.souvap-univention.de/souvap/tooling/charts/univention-keycloak/commit/a56d5c59fec5b1d93bad1f28f875dc0ddb9cbfdd))

## [1.0.2](https://gitlab.souvap-univention.de/souvap/tooling/charts/univention-keycloak/compare/v1.0.1...v1.0.2) (2023-11-24)


### Bug Fixes

* **limits:** Remove CPU limit ([c9f0296](https://gitlab.souvap-univention.de/souvap/tooling/charts/univention-keycloak/commit/c9f0296a3dd7aad04dca1894053e7a5663d6410a))

## [1.0.1](https://gitlab.souvap-univention.de/souvap/tooling/charts/univention-keycloak/compare/v1.0.0...v1.0.1) (2023-11-23)


### Bug Fixes

* **theme:** Theming and rename to ums-keycloak ([4da24c5](https://gitlab.souvap-univention.de/souvap/tooling/charts/univention-keycloak/commit/4da24c566cff5df47f4f4196ce976ac25ee8953d))

# 1.0.0 (2023-11-01)


### Bug Fixes

* **univention-keycloak:** Fix image digest ([1ce3b2d](https://gitlab.souvap-univention.de/souvap/tooling/charts/univention-keycloak/commit/1ce3b2d9ee3035fd95173fd6233da8bf1b6b94c3))


### Features

* **ci:** Add common CI builds ([438b4a4](https://gitlab.souvap-univention.de/souvap/tooling/charts/univention-keycloak/commit/438b4a4c99c362ff0501bf5c79212b27859898c2))
* **univention-keycloak:** Initial commit ([dc86bee](https://gitlab.souvap-univention.de/souvap/tooling/charts/univention-keycloak/commit/dc86beeb6fdd1b1b0dabaa1d4886a392fcbbcfbc))
